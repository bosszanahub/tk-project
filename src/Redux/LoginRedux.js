// Action
export const actions = {
    GET_LOGIN_REQUEST: 'GET_LOGIN_REQUEST',
    GET_LOGIN_SUCCESS: 'GET_LOGIN_SUCCESS',
    GET_LOGIN_FAILURE: 'GET_LOGIN_FAILURE',
    GET_LOGOUT_REQUEST: 'GET_LOGOUT_REQUEST',
    GET_LOGOUT_SUCCESS: 'GET_LOGOUT_SUCCESS',
    GET_LOGOUT_FAILURE: 'GET_LOGOUT_FAILURE',

    getLogin: () => ({
        type: actions.GET_LOGIN_REQUEST
    }),

    getLogout: () => ({
        type: actions.GET_LOGOUT_REQUEST
    })
}

// Initial state
export const INITIAL_STATE = {
    data: null,
    fetching: false,
    message: null
}

export default function loginReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case actions.GET_LOGIN_REQUEST:
            return { ...state, fetching: true }
        case actions.GET_LOGIN_SUCCESS:
            return { ...state, fetching: false }
        case actions.GET_LOGIN_FAILURE:
            return { ...state, fetching: false, message: action.message }
        case actions.GET_LOGOUT_REQUEST:
            return { ...state, fetching: true }
        case actions.GET_LOGOUT_SUCCESS:
            return { ...state, fetching: false }
        case actions.GET_LOGOUT_FAILURE:
            return { ...state, fetching: false, message: action.message }
        default:
            return state;
    }
}