import { createStore, combineReducers, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../Saga'

const sagaMiddleware = createSagaMiddleware()
const middlwwares = [sagaMiddleware]

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose;

const store = createStore(
    combineReducers({
        ...reducers
    }),
    composeEnhancers(applyMiddleware(...middlwwares))
)

sagaMiddleware.run(rootSaga)
export { store }