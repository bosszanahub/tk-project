// import Redux Actions
import actions from '../Redux/LoginRedux'
import { takeLatest, put, all, fork } from 'redux-saga/effects'
import auth from '../firebase'

export function* getLogut() {
    yield takeLatest(actions.GET_LOGOUT_REQUEST, function* () {
        auth.signOut().then(response => {
            yield put({
                type: actions.GET_LOGOUT_SUCCESS,
                data: null
            })
        }).catch(error => {
            yield put({
                type: actions.GET_LOGOUT_FAILURE,
                message: error.message
            })
        })
    })
}

export default function* rootSaga() {
    yield all([
        getLogut()
    ])
}