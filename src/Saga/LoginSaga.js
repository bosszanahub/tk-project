// import Redux Actions
import actions from '../Redux/LoginRedux'
import { takeLatest, put, all, fork } from 'redux-saga/effects'
import auth from '../firebase'

export function* getLogin() {
    yield takeLatest(actions.GET_LOGIN_REQUEST, function* () {
        auth.signInWithEmailAndPassword(email, password).then(response => {
            yield put({
                type: actions.GET_LOGIN_SUCCESS,
                data: response.user
            })
        }).catch(error => {
            yield put({
                type: actions.GET_LOGIN_FAILRURE,
                message: error.message
            })
        })
    })
}

export default function* rootSaga() {
    yield all([
        getLogin()
    ])
}